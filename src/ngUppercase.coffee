angular
  .module 'ngUppercase', []
  .directive 'ngUppercase', [
    ->
      {
        restrict: 'A'
        scope :
          ngModel : '='
        link : ( scope, elem, attrs ) ->

          elem.css 'text-transform', 'uppercase'

          elem.bind 'blur', ( ev ) ->
            scope.ngModel = scope.ngModel.toUpperCase() if scope.ngModel

      }
  ]

